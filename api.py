from time import time
from functools import wraps

from flask import Flask, request, jsonify, g
import mysql.connector as cnx

import constants as c


app = Flask(__name__)


### SQL FUNCTIONS
def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = cnx.connect(
            host=c.DB_HOST,
            user=c.DB_USER,
            passwd=c.DB_PASS,
            db=c.DB_NAME
        )
    return db


def q_ro(query, args=(), one=False):
    cur = get_db().cursor()
    cur.execute(query, args)
    result = cur.fetchall()
    cur.close()
    return (result[0] if result else []) if one else result


def q_wo(query, args=()):
    cur = get_db().cursor()
    cur.execute(query, args)
    cur.close()
    get_db().commit()


def is_service(service: str):
    if not bool(service):
        return False
    result = q_ro('SELECT count(name) FROM service WHERE name=%s;', (service,), one=True)
    return result[0] > 0


def service_alive(service: str):
    q_wo('UPDATE service SET last_alive=%s WHERE name=%s;', (int(time()), service))


def submit_error(service: str, error: str):
    q_wo('INSERT INTO error VALUES (DEFAULT, %s, %s, %s);', (service, error, int(time())))


def create_service(service: str, description: str):
    q_wo('INSERT INTO service VALUES (%s, %s, %s);', (service, description, int(time())))


def get_services():
    results = q_ro('SELECT * FROM service;')
    final = {}
    for result in results:
        final[result[0]] = {
            'description'   :   result[1],
            'last_alive'    :   result[2]
        }
    return final


def get_service_errors(service: str):
    final = {'errors' : []}
    if not bool(service):
        return final
    results = q_ro('SELECT description, _time FROM error WHERE name=%s ORDER BY _time DESC;', (service,))
    for result in results:
        final['errors'].append({
            'description'   :   result[0],
            'time'          :   result[1]
        })
    return final
###


def local_net(func):
    @wraps(func)
    def inner_wrapper(*args, **kwargs):
        if request.remote_addr in c.LOCAL_ADDRS:
            return func(*args, **kwargs)
        else:
            return 'Forbidden', 403
    return inner_wrapper


@app.route('/overview', methods=['GET'])
def overview():
    return jsonify(get_services())


@app.route('/geterrors', methods=['GET'])
def get_errors():
    name = request.args.get('name', None)
    if bool(name):
        return jsonify(get_service_errors(name))
    else:
        return jsonify({})


@app.route('/register', methods=['POST'])
@local_net
def register():
    name = request.form.get('name', None)
    desc = request.form.get('desc', None)
    if is_service(name):
        return 'OK'
    if bool(name):
        create_service(name, desc)
        return 'OK'
    else:
        return 'FAIL', 503


@app.route('/iamalive', methods=['POST'])
@local_net
def i_am_alive():
    name = request.form.get('name', None)
    if is_service(name):
        service_alive(name)
        return 'OK'
    else:
        return 'FAIL', 503


@app.route('/reporterror', methods=['POST'])
@local_net
def report_error():
    name = request.form.get('name', None)
    error = request.form.get('msg', None)
    error = request.form.get('levelname', None) + ' ' + error
    if is_service(name) and bool(error):
        submit_error(name, error)
        return 'OK'
    else:
        return 'FAIL', 503
