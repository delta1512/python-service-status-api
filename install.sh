#!/bin/bash

sudo useradd pssa
sudo passwd -l pssa
sudo mkdir /opt/pssa

if [ -f ./constants.py ] then
  sudo cp ./constants.py /opt/pssa/
else
  sudo cp ./constants.py.skel /opt/pssa/constants.py
fi

sudo cp api.py /opt/pssa/
sudo cp server.py /opt/pssa/
sudo cp pssa.py /usr/lib/python3.8/site-packages/
sudo cp pssa.service /usr/lib/systemd/system/

sudo systemctl daemon-reload

sudo chmod -w /usr/lib/python3.8/site-packages/pssa.py
sudo chown -R pssa:pssa /opt/pssa
sudo chmod -R -w /opt/pssa
sudo chmod -w /usr/lib/systemd/system/pssa.service

echo "Done..."
