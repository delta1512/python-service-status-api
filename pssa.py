'''
Main library script for the python service status API

Made by delta1512: https://gitlab.com/delta1512/python-service-status-api
'''

import logging
import requests
import threading
from time import sleep


# ACTIVE will be true if /iamalive POST request loop is active
ACTIVE = False
FAIL_CONN_MSG = 'PSSA: Could not connect to status API. Exiting...'
FAIL_INIT_MSG = 'PSSA: Something went wrong initiating PSSA. Exiting...'
SLEEP_TIME = 60

PORT = 40020
HOST = '127.0.0.1:{}'.format(PORT)
URL = '/reporterror'


# Test if the api is active
r = requests.get('http://{}/overview'.format(HOST))
try:
    assert r.status_code == 200
except AssertionError:
    logging.critical(FAIL_CONN_MSG)
    print(FAIL_CONN_MSG)
    exit(-1)
except Exception:
    logging.critical(FAIL_INIT_MSG)
    print(FAIL_INIT_MSG)
    exit(-2)


def get_handler(name: str, host=HOST, url=URL, description=''):
    pssa_handler = PSSAHandler(
        host=HOST,
        url=URL,
        method='POST',
        serviceName=name
    )

    # Set the handler to WARNINGS only
    pssa_handler.setLevel(30)

    r = requests.post('http://{}/register'.format(HOST), data={'name' : name, 'desc' : description})

    t = threading.Thread(target=main_loop, args=(name,))
    t.daemon = True
    t.start()

    return pssa_handler


def main_loop(name: str):
    while True:
        try:
            signal_iamalive(name)
        except AssertionError:
            logging.warning('PSSA: Failed to send iamalive signal')
        sleep(SLEEP_TIME)


def signal_iamalive(name: str):
    r = requests.post('http://{}/iamalive'.format(HOST), data={'name' : name})
    assert r.status_code == 200


class PSSAHandler(logging.Handler):
    """
    Copied from logging.handlers.HTTPHandler in order to change data fields in
    HTTP POST data. The notable change is adding the serviceName to the data
    field in the POST data
    """
    def __init__(self, host, url, serviceName, method="GET", secure=False,
                 credentials=None, context=None):
        """
        Initialize the instance with the host, the request URL, and the method
        ("GET" or "POST")
        """
        logging.Handler.__init__(self)
        method = method.upper()
        if method not in ["GET", "POST"]:
            raise ValueError("method must be GET or POST")
        if not secure and context is not None:
            raise ValueError("context parameter only makes sense "
                             "with secure=True")
        self.host = host
        self.url = url
        self.method = method
        self.secure = secure
        self.credentials = credentials
        self.context = context
        self.serviceName = serviceName

    def mapLogRecord(self, record):
        """
        Default implementation of mapping the log record into a dict
        that is sent as the CGI data. Overwrite in your class.
        Contributed by Franz Glasner.
        """
        return record.__dict__

    def emit(self, record):
        """
        Emit a record.

        Send the record to the Web server as a percent-encoded dictionary
        """
        try:
            import http.client, urllib.parse
            host = self.host
            if self.secure:
                h = http.client.HTTPSConnection(host, context=self.context)
            else:
                h = http.client.HTTPConnection(host)
            url = self.url
            data = self.mapLogRecord(record)
            # added the name field, required by the PSSA API
            data['name'] = self.serviceName
            data = urllib.parse.urlencode(data)
            if self.method == "GET":
                if (url.find('?') >= 0):
                    sep = '&'
                else:
                    sep = '?'
                url = url + "%c%s" % (sep, data)
            h.putrequest(self.method, url)
            # support multiple hosts on one IP address...
            # need to strip optional :port from host, if present
            i = host.find(":")
            if i >= 0:
                host = host[:i]
            # See issue #30904: putrequest call above already adds this header
            # on Python 3.x.
            # h.putheader("Host", host)
            if self.method == "POST":
                h.putheader("Content-type",
                            "application/x-www-form-urlencoded")
                h.putheader("Content-length", str(len(data)))
            if self.credentials:
                import base64
                s = ('%s:%s' % self.credentials).encode('utf-8')
                s = 'Basic ' + base64.b64encode(s).strip().decode('ascii')
                h.putheader('Authorization', s)
            h.endheaders()
            if self.method == "POST":
                h.send(data.encode('utf-8'))
            h.getresponse()    #can't do anything with the result
        except Exception:
            self.handleError(record)
