CREATE DATABASE IF NOT EXISTS pssa;


USE pssa;


-- Example user creation
-- CREATE USER 'pssa'@'localhost' IDENTIFIED BY 'password';
-- GRANT SELECT, INSERT, UPDATE ON pssa.* TO 'pssa'@'localhost';


CREATE TABLE IF NOT EXISTS service (
  name              CHAR(64),
  description       TINYTEXT,
  last_alive        BIGINT UNSIGNED,

  PRIMARY KEY (name)
);


CREATE TABLE IF NOT EXISTS error (
  eid               INT UNSIGNED AUTO_INCREMENT,
  name              CHAR(64),
  description       TEXT(2048),
  _time             BIGINT UNSIGNED,

  PRIMARY KEY (eid),
  FOREIGN KEY (name)  REFERENCES service (name)
);
