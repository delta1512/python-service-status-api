#!/bin/bash

# This script is for testing purposes only, use a WSGI server for production

export FLASK_APP=api.py
export FLASK_ENV=development
flask run --host=127.0.0.1 --port=40020 #--cert=cert.pem --key=key.pem
